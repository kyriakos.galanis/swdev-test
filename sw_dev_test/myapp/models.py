from django.db import models


class Employee(models.Model):
    employee_id = models.IntegerField()
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    favorite_color = models.CharField(max_length=120)

    objects = models.Manager()