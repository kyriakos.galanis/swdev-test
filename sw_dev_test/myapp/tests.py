import pytest
from django.test import RequestFactory
from myapp.views import *
from myapp.models import Employee


@pytest.fixture()
def employees():
    Employee.objects.create(employee_id=1, first_name="Bob", last_name="Dylan", favorite_color="black")
    Employee.objects.create(employee_id=2, first_name="Bill", last_name="Clinton", favorite_color="red")


@pytest.mark.django_db
class TestEmployeesList:

    def test_get_employees_list(self, employees):
        request_factory = RequestFactory()
        request = request_factory.get('/myapp/get_employees_list/')
        get_employees_list = EmployeesList()
        response = get_employees_list.get(request)

        assert response.status_code == 200
        assert response.data == {'employees': [{"employee_id": 1, "first_name": "Bob", "last_name": "Dylan",
                                                "favorite_color": "black"},
                                               {"employee_id": 2, "first_name": "Bill", "last_name": "Clinton",
                                                "favorite_color": "red"}]}


class TestGetEmployeeProject:

    def test_01_get_employee_project(self):
        pass