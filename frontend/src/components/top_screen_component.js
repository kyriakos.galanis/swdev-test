import React, { useEffect, useState } from "react"
const fetch_employees = () => {
    const url = 'http://127.0.0.1:8000/myapp/get_employees_list/'
    const headers = {
        'Content-Type': 'application/json',
    }
    return fetch(url, {
        method: 'GET',
        headers: headers,
    })
        .then(data => (
            data.json())
        )
        .then(data => {
            return data
        })
}

const TopScreenComponent = ({set_employee_id}) => {
    const [employees, set_employees] = useState([])

    useEffect(() => {
        console.log('fetching')
        fetch_employees()
            .then(data => set_employees(data['employees']))
    }, [])
    return (
        <div className='top_component_container'>
            {employees.length > 0 &&
                employees.map(employee => {
                    return (
                        <div
                            onClick={() => console.log('clicked')}
                            key={employee['employee_id']}
                            className="button"
                            >
                            {employee['first_name']} {employee['last_name']}
                        </div>
                    )
                })
            }
        </div>
    )
}

export default TopScreenComponent