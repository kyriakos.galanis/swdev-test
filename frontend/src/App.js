import './App.css';
import TopScreenComponent from './components/top_screen_component';
import { useState } from 'react';

function App() {
  const [employee_id, set_employee_id] = useState('1')
  return (
    <div className="App">
        <TopScreenComponent set_employee_id={set_employee_id}/>
        <div className='bottom_component_container'></div>
    </div>
  );
}

export default App;
